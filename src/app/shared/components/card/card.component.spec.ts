import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CardComponent } from './card.component';
import { HighlightSearchPipe } from '../../pipes/highlight-search.pipe';
import { EllipsisPipe } from '../../pipes/ellipsis.pipe';

describe('CardComponent', () => {
  let component: CardComponent;
  let fixture: ComponentFixture<CardComponent>;
  let emailObj = {
    "from": "k..allen@enron.com",
    "to": [
      "c..gossett@enron.com"
    ],
    "cc": [],
    "bcc": [],
    "subject": "",
    "body": "Jeff,\n\nAre the books and VAR set up for simulated trading?  We began doing trades on the user id\u0027s set up by jay webb.\n\nPhillip",
    "date": "2001-07-17T20:32:49.000+0000"
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CardComponent, HighlightSearchPipe, EllipsisPipe ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should component be defnied', () => {
    expect(component).toBeTruthy();
  });

  it('should emit email Object on click of the email Card item', () => {
    component.cardData = emailObj;
    spyOn(component.selectEmail, 'emit');
    fixture.detectChanges();

    let emailItemCard = fixture.debugElement.nativeElement.querySelector('.email-item');
    emailItemCard.click();

    expect(component.selectEmail.emit).toHaveBeenCalledWith(emailObj);  
  });
});
