import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { EmailObject } from '../../models/MailResponse';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {

  @Input() cardData?: EmailObject;
  @Input() searchText: string;
  @Output() selectEmail: EventEmitter<EmailObject> = new EventEmitter<EmailObject>();
  @Input() selectedCardIndex: number;

  constructor() { 
  }

  ngOnInit(): void {
  }

  onSelectEmail(email: EmailObject) {
    this.selectEmail.emit(email);
  }

}
