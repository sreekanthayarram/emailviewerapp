import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderComponent } from './header.component';
import { FirstLetterWordPipe } from '../../pipes/first-letter-word.pipe';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

describe('HeaderComponent', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ FontAwesomeModule],
      declarations: [ HeaderComponent, FirstLetterWordPipe ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should HeaderComponent defined', () => {
    expect(component).toBeTruthy();
  });

  it('should emit searched value from search input', () => {
    spyOn(component.searched, 'emit');
    const searchInputEle = fixture.nativeElement.querySelector('input');
    searchInputEle.value = "Your";    
    searchInputEle.dispatchEvent(new Event('input'));
    fixture.detectChanges();

    expect(component.searched.emit).toHaveBeenCalledWith('Your');
  });
});
