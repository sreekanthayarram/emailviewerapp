import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { faPowerOff, faSearch, faUser } from "@fortawesome/free-solid-svg-icons"

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  faPowerOff = faPowerOff;
  faSearch = faSearch;
  faUser = faUser;
  name: string = "Sreekanth Reddy";

  @Output() searched = new EventEmitter<string>();

  constructor() { }

  ngOnInit(): void {
  }

  public onSearch(searchTerm: string): void {
    this.searched.emit(searchTerm);
  }

}
