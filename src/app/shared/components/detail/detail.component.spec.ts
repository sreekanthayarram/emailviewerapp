import { ComponentFixture, TestBed, async } from '@angular/core/testing';

import { DetailComponent } from './detail.component';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatTooltipModule } from '@angular/material/tooltip';
import { HighlightSearchPipe } from '../../pipes/highlight-search.pipe';
import { By } from '@angular/platform-browser';
import { NgxLinkifyjsModule } from 'ngx-linkifyjs';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('DetailComponent', () => {
  let component: DetailComponent;
  let fixture: ComponentFixture<DetailComponent>;
  let emailObj = {
    "from": "k..allen@enron.com",
    "to": [
      "c..gossett@enron.com"
    ],
    "cc": [],
    "bcc": [],
    "subject": "",
    "body": "Jeff,\n\nAre the books and VAR set up for simulated trading?  We began doing trades on the user id\u0027s set up by jay webb.\n\nPhillip",
    "date": "2001-07-17T20:32:49.000+0000"
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [MatSnackBarModule, FontAwesomeModule, MatIconModule, MatMenuModule, MatTooltipModule, NgxLinkifyjsModule.forRoot(), BrowserAnimationsModule],
      declarations: [ DetailComponent, HighlightSearchPipe ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailComponent);
    component = fixture.componentInstance;
    component.bodyData =  emailObj;
    fixture.detectChanges();
  });

  it('should component to be defined', () => {
    expect(component).toBeTruthy();
  });
  
  it('hould scroll to top when message body is changed', () => {
    component.bodyData =  {
      "from": "kllen@enron.com",
      "to": [
        "c..gossett@enron.com"
      ],
      "cc": [],
      "bcc": [],
      "subject": "",
      "body": "Jeff,\n\nAre the books and VAR set up for simulated trading?  We began doing trades on the user id\u0027s set up by jay webb.\n\nPhillip",
      "date": "2001-07-17T20:32:49.000+0000"
    };
    component.ngOnChanges({});
    expect(component).toBeTruthy();
  });

  it('should call toggleStar on star icon click', async(() => {
    spyOn(component, 'toggleStar');
  
    let starIcon = fixture.debugElement.nativeElement.querySelector('.star');
    starIcon.click();
  
    fixture.whenStable().then(() => {
      expect(component.toggleStar).toHaveBeenCalled();
    });
  }));

  it('should toggle star class on click of the star mat-icon ', () => {

    let starEle = fixture.debugElement.query(By.css('.star'));
    component.isActive = false;
    component.toggleStar();
    fixture.detectChanges();

    expect(starEle.nativeElement.classList).toContain('active');
  });

  it('should call deleteThisMail', async(() => {
    component.deleteThisMail(emailObj);    
  }));

  it('should call deleteThisMail', async(() => {
    spyOn(component, 'deleteThisMail');
  
    let deleteMailIcon = fixture.debugElement.nativeElement.querySelector('.deleteMail');
    deleteMailIcon.click();
  
    fixture.whenStable().then(() => {
      expect(component.deleteThisMail).toHaveBeenCalled();
    });
  }));

  it('should call deleteThisMail', async(() => {
    spyOn(component.snackBar, 'open');
    component.handleEmailOption('Reply');

    expect(component.snackBar.open).toHaveBeenCalled();
  }));
});
