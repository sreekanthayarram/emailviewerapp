import { Component, OnInit, Input, AfterViewInit, OnChanges, SimpleChanges } from '@angular/core';
import { EmailObject } from '../../models/MailResponse';
import { faEnvelope, faStar, faReply, faPrint } from "@fortawesome/free-solid-svg-icons"
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit, OnChanges {

  faEnvelope = faEnvelope;
  faStar = faStar;
  faReply = faReply;
  faPrint = faPrint;

  emailMenuOptions = ["Reply", "Reply All", "Forward", "Report Spam", "Block"];

  @Input() bodyData?: EmailObject;
  @Input() searchText: string;
  constructor(public snackBar: MatSnackBar) { }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {
    let top = document.querySelector('.message');
    if (top !== null) {
      top.scrollIntoView();
      top = null;
    }
  }

  handleEmailOption(emailOption: string){
    this.snackBar.open("Handler of: "+ emailOption, 'Done', { duration: 2000 });
  }

  isActive: boolean = false;
  toggleStar(){
    this.isActive = !this.isActive;
  }

  deleteThisMail(email: EmailObject){
    console.log("Deleting Email: ", email);
  }

}
