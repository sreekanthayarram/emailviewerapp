export interface MailResponse{
    mails: Array<EmailObject>
}

export interface EmailObject{
    index?: number,
    from: string,
    to: Array<string>,
    cc: Array<string>,
    bcc: Array<string>,
    subject?: string,
    body: string,
    date: string    
}

