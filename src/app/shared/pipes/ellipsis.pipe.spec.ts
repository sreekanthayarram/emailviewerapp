import { EllipsisPipe } from './ellipsis.pipe';

describe('EllipsisPipe', () => {
  let ellipsisPipe;
  beforeEach(()=> {
    ellipsisPipe = new EllipsisPipe();
  })
  
  it('EllipsisPipe should be defined', () => {
    expect(ellipsisPipe).toBeTruthy();
  });

  it('Should trim the text, add ... at end,  if length of text more than the specified number', () => {
    let ellipsisText = ellipsisPipe.transform('mery.brownl.brown@accenture.com', 20);
    expect(ellipsisText).toContain("...");
  });

  it('Should trim the text, add ... at end,  if length of text more than the default number', () => {
    let ellipsisText = ellipsisPipe.transform('mery.brownl.brown@accenture.com');
    expect(ellipsisText).toContain("...");
  });

  it('Should visible full text if specified number less than the length of the text', () => {
    let ellipsisText = ellipsisPipe.transform('mery.brown@accenture.com', 30);
    expect(ellipsisText).toContain("mery.brown@accenture.com");
  });

  it('Should return empty text if text is passed or null', () => {
    let ellipsisText = ellipsisPipe.transform(undefined);
    expect(ellipsisText).toBe("");
  });
});
