import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'firstLetterWord'
})
export class FirstLetterWordPipe implements PipeTransform {

  transform(value: string, ...args: any[]): string {
    let matches = value.match(/\b(\w)/g) || [];
    return matches.join('').slice(0, 2);
  }
}
