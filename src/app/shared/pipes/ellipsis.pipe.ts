import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'ellipsis'
})
export class EllipsisPipe implements PipeTransform {

  transform(value: string="", limitTo: number = 25) {
    if (value.length > limitTo) {
      return value.substring(0, limitTo) + ' ...';
    } else {
      return value;
    }
  }

}

