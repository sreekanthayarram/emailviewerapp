import { FirstLetterWordPipe } from './first-letter-word.pipe';

describe('FirstLetterWordPipe', () => {

  let letterWordPipe;
  beforeEach(()=> {
    letterWordPipe = new FirstLetterWordPipe();
  })

  it('FirstLetterWordPipe should be defined', () => {
    expect(letterWordPipe).toBeTruthy();
  });

  it('FirstLetterWordPipe should return first letter of each word', () => {
    let transformedText = letterWordPipe.transform('Sreekanth Reddy');
    expect(transformedText).toBe('SR');
  });

  it('FirstLetterWordPipe should return only first letter of starting two words', () => {
    let transformedText = letterWordPipe.transform('Sreekanth Reddy Yarram');
    expect(transformedText).toBe('SR');
  });

  it('FirstLetterWordPipe should return only first letter if its single word', () => {
    let transformedText = letterWordPipe.transform('Sreekanth');
    expect(transformedText).toBe('S');
  });

  it('FirstLetterWordPipe should return empty string if No words', () => {
    let transformedText = letterWordPipe.transform('');
    expect(transformedText).toBe('');
  });

});
