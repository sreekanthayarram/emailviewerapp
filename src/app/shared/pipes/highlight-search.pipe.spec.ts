import { HighlightSearchPipe } from './highlight-search.pipe';

describe('HighlightSearchPipe', () => {
  let highlightSearchPipe;
  beforeEach(()=> {
    highlightSearchPipe = new HighlightSearchPipe();
  })
  it('HighlightSearchPipe should be defined', () => {
    expect(highlightSearchPipe).toBeTruthy();
  });

  it('HighlightSearchPipe should be undefined if No value or Args passed', () => {
    let transformedValue = highlightSearchPipe.transform();
    expect(transformedValue).toBeUndefined();
  });

  it('HighlightSearchPipe should contain highlight class if text arg passed and matched in value', () => {

    let transformedValue = highlightSearchPipe.transform("This is the search text", 'text');
    expect(transformedValue).toContain('highlight');
  });

  it('HighlightSearchPipe should not contain highlight class if text arg passed and not matched in value', () => {

    let transformedValue = highlightSearchPipe.transform("This is the search text", 'randomtext');
    expect(transformedValue).not.toContain('highlight');
  });

});
