import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './components/header/header.component';
import { CardComponent } from './components/card/card.component';
import { DetailComponent } from './components/detail/detail.component';
import { EllipsisPipe } from './pipes/ellipsis.pipe';
import { HighlightSearchPipe } from './pipes/highlight-search.pipe';
import { NgxLinkifyjsModule } from 'ngx-linkifyjs';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { MaterialModule } from '../material.module';
import { FirstLetterWordPipe } from './pipes/first-letter-word.pipe';
import { MatFormFieldModule } from '@angular/material/form-field';

@NgModule({
  declarations: [
    EllipsisPipe,
    HighlightSearchPipe,
    CardComponent,
    HeaderComponent,
    DetailComponent,
    FirstLetterWordPipe
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgxLinkifyjsModule.forRoot(),
    FontAwesomeModule,
    MaterialModule,
    MatFormFieldModule
  ],
  exports: [
    CardComponent,
    HeaderComponent,
    DetailComponent,
    EllipsisPipe,
    HighlightSearchPipe,
    FirstLetterWordPipe,
    CommonModule, 
    FormsModule
  ]
})
export class SharedModule { }
