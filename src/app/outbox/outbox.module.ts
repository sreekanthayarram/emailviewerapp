import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OutboxRoutingModule } from './outbox-routing.module';
import { OutboxComponent } from './outbox.component';
import { SharedModule } from '../shared/shared.module';


@NgModule({
  declarations: [OutboxComponent],
  imports: [
    CommonModule,
    OutboxRoutingModule,
    SharedModule
  ]
})
export class OutboxModule { }
