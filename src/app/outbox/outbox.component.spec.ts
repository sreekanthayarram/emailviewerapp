import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OutboxComponent } from './outbox.component';
import { HeaderComponent } from '../shared/components/header/header.component';
import { FirstLetterWordPipe } from '../shared/pipes/first-letter-word.pipe';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

describe('OutboxComponent', () => {
  let component: OutboxComponent;
  let fixture: ComponentFixture<OutboxComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ FontAwesomeModule],
      declarations: [ OutboxComponent, HeaderComponent, FirstLetterWordPipe ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OutboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
