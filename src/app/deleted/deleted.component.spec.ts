import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeletedComponent } from './deleted.component';
import { HeaderComponent } from '../shared/components/header/header.component';
import { FirstLetterWordPipe } from '../shared/pipes/first-letter-word.pipe';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

describe('DeletedComponent', () => {
  let component: DeletedComponent;
  let fixture: ComponentFixture<DeletedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DeletedComponent, HeaderComponent, FirstLetterWordPipe ],
      imports: [ FontAwesomeModule]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeletedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
