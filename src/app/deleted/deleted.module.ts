import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DeletedRoutingModule } from './deleted-routing.module';
import { DeletedComponent } from './deleted.component';
import { SharedModule } from '../shared/shared.module';


@NgModule({
  declarations: [DeletedComponent],
  imports: [
    CommonModule,
    DeletedRoutingModule,
    SharedModule
  ]
})
export class DeletedModule { }
