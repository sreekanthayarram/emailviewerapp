import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-compose',
  templateUrl: './compose.component.html',
  styleUrls: ['./compose.component.scss']
})
export class ComposeComponent implements OnInit {

  composeMailForm: FormGroup;
  ccField = new FormControl(false);
  bccField = new FormControl(false);
  
  constructor(private formBuilder: FormBuilder, public snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.composeMailForm = this.formBuilder.group({
      to: [null, [Validators.required, Validators.email]],
      cc: [null, [Validators.email]],
      bcc: [null, [Validators.email]],
      subject: [null, Validators.required],
      body: [null, Validators.required]
    });
  }

  submitMail(){
    console.log(this.composeMailForm.value);
    this.snackBar.open("Email Sent: "+ JSON.stringify(this.composeMailForm.value), 'Done', { duration: 2000 });
    this.composeMailForm.reset();    
  }

  saveInDrafts(){
    this.snackBar.open("Saved in Drafts: "+ JSON.stringify(this.composeMailForm.value), 'Done', { duration: 2000 });
    this.composeMailForm.reset();  
  }

}
