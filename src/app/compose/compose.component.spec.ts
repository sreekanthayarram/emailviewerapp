import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ComposeComponent } from './compose.component';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { HeaderComponent } from '../shared/components/header/header.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { FirstLetterWordPipe } from '../shared/pipes/first-letter-word.pipe';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatSnackBarModule } from '@angular/material/snack-bar';

describe('ComposeComponent', () => {
  let component: ComposeComponent;
  let fixture: ComponentFixture<ComposeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ FontAwesomeModule, ReactiveFormsModule, MatFormFieldModule, MatInputModule, NoopAnimationsModule, MatCheckboxModule, MatSnackBarModule ],
      declarations: [ ComposeComponent, HeaderComponent, FirstLetterWordPipe ],
      providers: [
        FormBuilder        
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ComposeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('component should be defined', () => {
    expect(component).toBeTruthy();
  });
  
  it('Form Should be valid  when all the required email fields are entered', () => {
    spyOn(component.composeMailForm, 'reset');
    component.composeMailForm.controls['to'].setValue("test@email.com");
    component.composeMailForm.controls['subject'].setValue("sample subject");
    component.composeMailForm.controls['body'].setValue("Email Body");
    let submitMailEle = fixture.debugElement.nativeElement.querySelector('.submit-mail');
    
    fixture.detectChanges();

    expect(component.composeMailForm.valid).toBeTruthy();
    expect(submitMailEle.disabled).toBeFalsy();
  });
  
  it('Form should be invalid when any one of the required fields are not entered', () => {
    component.composeMailForm.controls['to'].setValue("test@email.com");
    component.composeMailForm.controls['subject'].setValue("sample subject");
    component.composeMailForm.controls['body'].setValue("");
    let submitMailEle = fixture.debugElement.nativeElement.querySelector('.submit-mail');
    
    fixture.detectChanges();

    expect(component.composeMailForm.invalid).toBeTruthy();
    expect(submitMailEle.disabled).toBeTruthy();
  });

  it('Form should be submitted and reset on Mail submit', () => {
    
    spyOn(component.composeMailForm, 'reset');
    spyOn(component.snackBar, 'open');
    component.composeMailForm.controls['to'].setValue("test@email.com");
    component.composeMailForm.controls['subject'].setValue("sample subject");
    component.composeMailForm.controls['body'].setValue("Email Body");
    component.submitMail();
    fixture.detectChanges();

    expect(component.snackBar.open).toHaveBeenCalled();
    expect(component.composeMailForm.reset).toHaveBeenCalled();
  });

  it('Form should be saved in drafts when Save in Drafts is called', () => {
    
    spyOn(component.composeMailForm, 'reset');
    spyOn(component.snackBar, 'open');
    component.composeMailForm.controls['to'].setValue("test@email.com");
    component.composeMailForm.controls['subject'].setValue("sample subject");
    component.composeMailForm.controls['body'].setValue("Email Body");
    component.saveInDrafts();
    fixture.detectChanges();

    expect(component.snackBar.open).toHaveBeenCalled();
    expect(component.composeMailForm.reset).toHaveBeenCalled();
  });
});
