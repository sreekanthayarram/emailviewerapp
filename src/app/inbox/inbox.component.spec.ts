import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InboxComponent } from './inbox.component';
import { DataService } from '../services/data.service';
import { of } from 'rxjs';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material/select';
import { HeaderComponent } from '../shared/components/header/header.component';
import { MatPaginatorModule, PageEvent } from '@angular/material/paginator';
import { CardComponent } from '../shared/components/card/card.component';
import { DetailComponent } from '../shared/components/detail/detail.component';
import { FirstLetterWordPipe } from '../shared/pipes/first-letter-word.pipe';
import { HighlightSearchPipe } from '../shared/pipes/highlight-search.pipe';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MOCK_RESPONSE } from "./mockResponse";
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { EllipsisPipe } from '../shared/pipes/ellipsis.pipe';
import { LABEL_CONSTANTS } from '../app.constants';

describe('InboxComponent', () => {
  let component: InboxComponent;
  let fixture: ComponentFixture<InboxComponent>;

  let mockDataService = jasmine.createSpyObj(DataService, ['getMailResponse', 'getMailsBySortOption']);

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [FontAwesomeModule, ReactiveFormsModule, MatSelectModule, MatPaginatorModule, BrowserAnimationsModule, MatSnackBarModule],
      declarations: [ InboxComponent, HeaderComponent, CardComponent, DetailComponent, FirstLetterWordPipe, HighlightSearchPipe, EllipsisPipe ],
      providers: [{
        provide: DataService, useValue: mockDataService
      }]
    })
    .compileComponents();
    mockDataService.getMailResponse.and.returnValue(of(MOCK_RESPONSE));
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should show message body on selection of Email card', () => {
    let emailObj = MOCK_RESPONSE[0];
    component.onSelectEmail(emailObj);

    expect(component.messageBody).toBe(emailObj);
  });

  it('should show the email results of matched text in subject or body', () => {
    component.OnSearched('Your');

    expect(component.selectedEmailIndex).toBe(undefined);
    expect(component.messageBody).toBe(undefined);
    expect(component.searchText).toBe('Your');
  });


  it('should update emails when sorting option cahnged', () => {
    mockDataService.getMailsBySortOption.and.returnValue(of(MOCK_RESPONSE));
    component.onSelectionChange({value: LABEL_CONSTANTS.OLDEST_ON_TOP});
    
    expect(mockDataService.getMailsBySortOption).toHaveBeenCalledWith(LABEL_CONSTANTS.OLDEST_ON_TOP);
  });

  it('should update emails when Pagination controls changed', () => {
    let mockPageEvent: PageEvent = {
      pageIndex: 0,
      pageSize: 20,
      length: MOCK_RESPONSE.length
    }
    component.onPageChange(mockPageEvent);
    
    expect(component.pageSize).toBe(mockPageEvent.pageSize);
    expect(component.pageIndex).toBe(mockPageEvent.pageIndex);
    expect(component.paginationResultsCount).toBe(mockPageEvent.length);
  });
});
