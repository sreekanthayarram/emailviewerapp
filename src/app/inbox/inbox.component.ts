import { Component, OnInit, ViewChild } from '@angular/core';
import { DataService } from '../services/data.service';
import { EmailObject } from '../shared/models/MailResponse';
import { faSearch } from '@fortawesome/free-solid-svg-icons'
import { PageEvent, MatPaginator } from '@angular/material/paginator';
import { FormControl } from '@angular/forms';
import { LABEL_CONSTANTS } from '../app.constants';

@Component({
  selector: 'app-inbox',
  templateUrl: './inbox.component.html',
  styleUrls: ['./inbox.component.scss']
})
export class InboxComponent implements OnInit {
  
  faSearch = faSearch;
  
  actualEmailsList?: EmailObject[];
  
  totalFilteredEmails?: EmailObject[];
  filteredEmailsPerPage?: EmailObject[];
  messageBody?: EmailObject;
  
  selectedEmailIndex: number;
  searchText: string = '';
  
  paginationResultsCount: number;
  pageSize: number = 20;
  pageIndex: number = 0;
  pageEvent: PageEvent;

  filterOptions = [LABEL_CONSTANTS.NEWEST_ON_TOP, LABEL_CONSTANTS.OLDEST_ON_TOP];
  filterControl = new FormControl('Newest on top', []);
  
  @ViewChild(MatPaginator, { static : false}) paginator: MatPaginator;
  
  constructor(private dataService: DataService){}
  
  ngOnInit(): void {
    this.dataService.getMailResponse().subscribe((data: EmailObject[]) => {
      this.actualEmailsList = data;
      this.totalFilteredEmails = data;
      this.refreshPaginationControls();
    });
  }
  
  trackByIndex(email: EmailObject){
    return email.index;
  }
  
  onSelectEmail(email: EmailObject) {
    this.selectedEmailIndex = email.index;
    this.messageBody = email;
  }
  
  OnSearched(searchTerm: string) {
    this.messageBody = undefined;
    this.selectedEmailIndex = undefined;    
    this.searchText = searchTerm;
    this.paginator.firstPage();
    this.refreshPaginationControls();
  }
  
  onPageChange(e: PageEvent): void {
    this.pageSize = e.pageSize;
    this.pageIndex = e.pageIndex;
    this.refreshPaginationControls();    
  }
  
  refreshPaginationControls(){
    this.totalFilteredEmails = [...this.actualEmailsList].filter(emailItem => emailItem.subject.indexOf(this.searchText) > -1 || emailItem.body.indexOf(this.searchText) > -1 );
    this.paginationResultsCount = this.totalFilteredEmails.length;
    this.loadData(this.pageIndex, this.pageSize);
  }
  
  private loadData(pageIndex: number, pageSize: number, paginationList: EmailObject[] = [...this.totalFilteredEmails]): void {
    this.filteredEmailsPerPage = paginationList.slice(pageIndex * pageSize, (pageIndex * pageSize) + pageSize);
  }

  onSelectionChange(sortOption){
    this.dataService.getMailsBySortOption(sortOption.value).subscribe((data: EmailObject[]) => {
      this.actualEmailsList = data;
      this.totalFilteredEmails = data;
      this.refreshPaginationControls();
    })
  }
  
}
