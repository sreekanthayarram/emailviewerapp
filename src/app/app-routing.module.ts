import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';


const routes: Routes = [{ path: '', redirectTo: '/inbox', pathMatch: 'full' },
{ path: 'sent', loadChildren: () => import('./sent/sent.module').then(m => m.SentModule) },
{ path: 'outbox', loadChildren: () => import('./outbox/outbox.module').then(m => m.OutboxModule) },
{ path: 'drafts', loadChildren: () => import('./drafts/drafts.module').then(m => m.DraftsModule) },
{ path: 'deleted', loadChildren: () => import('./deleted/deleted.module').then(m => m.DeletedModule) },
{ path: 'inbox', loadChildren: () => import('./inbox/inbox.module').then(m => m.InboxModule) },
{ path: 'compose', loadChildren: () => import('./compose/compose.module').then(m => m.ComposeModule) },
{ path: '**', redirectTo: '/inbox' }];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
