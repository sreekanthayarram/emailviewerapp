import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SentComponent } from './sent.component';
import { HeaderComponent } from '../shared/components/header/header.component';
import { FirstLetterWordPipe } from '../shared/pipes/first-letter-word.pipe';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

describe('SentComponent', () => {
  let component: SentComponent;
  let fixture: ComponentFixture<SentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [FontAwesomeModule],
      declarations: [ SentComponent, HeaderComponent, FirstLetterWordPipe ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
