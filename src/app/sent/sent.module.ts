import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SentRoutingModule } from './sent-routing.module';
import { SentComponent } from './sent.component';
import { SharedModule } from '../shared/shared.module';


@NgModule({
  declarations: [SentComponent],
  imports: [
    CommonModule,
    SentRoutingModule,
    SharedModule
  ]
})
export class SentModule { }
