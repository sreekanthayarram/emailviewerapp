import { Component, OnInit } from '@angular/core';
import { faInbox, faSignOutAlt, faTrash, faPencilAlt, faPaperPlane, faCog, faPlusCircle} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-main-nav',
  templateUrl: './main-nav.component.html',
  styleUrls: ['./main-nav.component.scss']
})
export class MainNavComponent implements OnInit {
  faInbox = faInbox;
  faTrash = faTrash;
  faPaperPlane = faPaperPlane;
  faSignOutAlt = faSignOutAlt;
  faPencilAlt=faPencilAlt;
  faCog=faCog;
  faPlusCircle=faPlusCircle;
  
  constructor() { }
  
  ngOnInit(): void {
  }
}
