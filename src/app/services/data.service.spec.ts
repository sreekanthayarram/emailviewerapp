import { TestBed, tick } from '@angular/core/testing';
import {HttpTestingController, HttpClientTestingModule} from '@angular/common/http/testing';
import { DataService } from './data.service';
import { EmailObject } from '../shared/models/MailResponse';
import { LABEL_CONSTANTS } from '../app.constants';
import { of } from 'rxjs';

describe('DataService', () => {
  let service: DataService;
  let httpTestingController: HttpTestingController;
  let mockResponse;

  beforeEach(() => {

    mockResponse = [
      { "from": "k..allen@enron.com", "to": [ "a..martin@enron.com" ], "cc": [], "bcc": [], "subject": "FW:", "body": "Phillip Allen", "date": "2001-11-06T13:18:49.000+0000" }, 
      { "from": "allen@enron.com", "to": [ "c..gossett@enron.com" ], "cc": [], "bcc": [], "subject": "", "body": "old email body", "date": "2000-07-17T20:32:49.000+0000" }
    ]
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [DataService]
    });
    service = TestBed.inject(DataService);
    httpTestingController = TestBed.get(HttpTestingController);
  });

  afterEach(() => {
    httpTestingController.verify();
    service.response$ = null;
  });

  it('service to be defined', () => {
    expect(service).toBeTruthy();    
  });

  it("should return mail response data", () => {
    let result: EmailObject[];
    service.getMailResponse().subscribe(data => {
      result = data;
    });
    const req = httpTestingController.expectOne({
      method: "GET",
      url: 'assets/mocks/email_2.json'
    });
    expect(req.request.method).toEqual('GET');
    req.flush(mockResponse);
   
    expect(result[0].cc).toEqual(mockResponse[0].cc);
    expect(result[0].to).toEqual(mockResponse[0].to);
    expect(result[0].from).toEqual(mockResponse[0].from);
    expect(result[0].subject).toEqual(mockResponse[0].subject);
    
  });


  it("should return cached mail response data if already response received", () => {
    let result: EmailObject[];
    service.getMailResponse().subscribe(data => {
      result = data;
    });
    const req = httpTestingController.expectOne({
      method: "GET",
      url: 'assets/mocks/email_2.json'
    });
   
    req.flush(mockResponse);

    service.getMailResponse().subscribe(data => {
      result = data;
    });
    const req2 = httpTestingController.expectOne({
      method: "GET",
      url: 'assets/mocks/email_2.json'
    });
    expect(req.request.method).toEqual('GET');
    req2.flush(mockResponse);
   
    expect(result[0].cc).toEqual(mockResponse[0].cc);
    expect(result[0].to).toEqual(mockResponse[0].to);
    expect(result[0].from).toEqual(mockResponse[0].from);
    expect(result[0].subject).toEqual(mockResponse[0].subject);
  });

  it('getMailsBySortOption oldest mail should come first', async(done) => {
    mockResponse = [
      { "from": "k..allen@enron.com", "to": [ "a..martin@enron.com" ], "cc": [], "bcc": [], "subject": "FW:", "body": "Phillip Allen", "date": "2001-11-06T13:18:49.000+0000" }, 
      { "from": "allen@enron.com", "to": [ "c..gossett@enron.com" ], "cc": [], "bcc": [], "subject": "", "body": "old email body", "date": "2000-07-17T20:32:49.000+0000" }
    ]
    service.getMailsBySortOption(LABEL_CONSTANTS.OLDEST_ON_TOP).subscribe((sortedData) => {
      expect(new Date(sortedData[0].date).getFullYear()).toBeLessThanOrEqual(new Date(mockResponse[0].date).getFullYear());
      done();
    });

    const req2 = httpTestingController.expectOne({
      method: "GET",
      url: 'assets/mocks/email_2.json'
    });
    req2.flush(mockResponse);
  })

  it('getMailsBySortOption Newest mails first test', async(done) => {
    mockResponse = [
      { "from": "k..allen@enron.com", "to": [ "a..martin@enron.com" ], "cc": [], "bcc": [], "subject": "FW:", "body": "Phillip Allen", "date": "2001-11-06T13:18:49.000+0000" }, 
      { "from": "allen@enron.com", "to": [ "c..gossett@enron.com" ], "cc": [], "bcc": [], "subject": "", "body": "old email body", "date": "2000-07-17T20:32:49.000+0000" }
    ]
    service.response$ = of(mockResponse);
    service.getMailsBySortOption(LABEL_CONSTANTS.NEWEST_ON_TOP).subscribe((sortedData) => {
      expect(new Date(sortedData[0].date).getFullYear()).toBeLessThanOrEqual(new Date(mockResponse[0].date).getFullYear());
      done();
    });
  })
  
});
