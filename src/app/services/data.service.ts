import { Injectable } from '@angular/core';
import { Observable, of, from, zip } from 'rxjs';
import { map, groupBy, mergeMap, toArray } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { EmailObject } from '../shared/models/MailResponse';
import { LABEL_CONSTANTS } from '../app.constants';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  
  constructor(private http: HttpClient) { }
  
  response$: Observable<EmailObject[]>;
  
  getMailResponse(): Observable<EmailObject[]>{
    if(!this.response$){
      this.response$ = this.http.get<EmailObject[]>("assets/mocks/email_2.json").pipe(
        map(mailObject => mailObject.sort((a, b) => new Date(b.date).getTime() - new Date(a.date).getTime())),
        map(mailObject => mailObject.map((emailObj, index) => {
          emailObj['index'] = index;
          return emailObj;
        })))
      } 
      return this.response$;
    }

    getMailsBySortOption(sortedOption): Observable<EmailObject[]>{
        if(sortedOption == LABEL_CONSTANTS.OLDEST_ON_TOP){
          return this.getMailResponse().pipe(map(mailObject => mailObject.sort((a, b) => new Date(a.date).getTime() - new Date(b.date).getTime())),
            map(mailObject => mailObject.map((emailObj, index) => {
              emailObj['index'] = index;
              return emailObj;
            })));
        }
        return this.response$;
      }
    
    /* groupByFieldName(fieldName: string){
      return this.getMailResponse().pipe(
        mergeMap(mailObjectArr => mailObjectArr),
        groupBy(mailObject => mailObject[fieldName], p => p),
        mergeMap(group => zip(of(group.key), group.pipe(toArray()))));
      }
    */
      
    }
